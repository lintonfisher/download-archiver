import getpass
import logging
import os
from datetime import datetime


def clean_downloads_folder(archive_age):
    # Get paths and variables
    username = getpass.getuser()
    downloads_folder = 'C:\\Users\\%s\\Downloads' % username
    archives_folder = '%s\\Archives' % downloads_folder
    logs_folder = '%s\\logs' % archives_folder
    current_time = datetime.now()
    log_file = '%s\\logs\\%s%s%s_%s%s.log' % (archives_folder,
                                              current_time.year, current_time.month, current_time.day,
                                              current_time.hour, current_time.minute)

    # Create the Archives folder if it does not existUsers
    if not os.path.isdir(archives_folder):
        os.mkdir(archives_folder)
        logging.info('Created Archives directory.')
        # Create the logs folder if it does not exist
        if not os.path.isdir(logs_folder):
            os.mkdir(logs_folder)
            logging.info('Created Logs directory.')

    # Configure the logging agent
    logging.basicConfig(filename=log_file, level=logging.DEBUG)

    # Log some variables
    logging.info('Current user: %s' % username)
    logging.info('Current Time: %s' % current_time)

    # Get folder items
    downloads_items = os.listdir(downloads_folder)

    # Move old items to Archives
    downloads_items.remove('Archives')
    if len(downloads_items) > 0:
        for item in downloads_items:
            if not item.startswith('.'):
                f_path = '%s/%s' % (downloads_folder, item)
                fa_path = '%s/%s' % (archives_folder, item)
                f_age = current_time - \
                        datetime.fromtimestamp(os.path.getmtime(f_path))
                if int(f_age.days) > archive_age:
                    try:
                        os.rename(f_path, fa_path)
                        logging.info('Moved %s to Archives' % item)
                    except Exception as E:
                        logging.error('Unable to move %s to Archives' % item)
                        logging.error(E)
                else:
                    logging.info('%s not moved. Age is: %s' % (item, f_age))
    else:
        logging.info('No files in downloads folder')


if __name__ == "__main__":
    # cleanDownloadsFolder(a, b)
    # a: Files in /Downloads not modified for this number of days are moved to /Downloads/Archives
    # b: Files in /Downloads/Archives not modified for this number of days are deleted
    clean_downloads_folder(7)
